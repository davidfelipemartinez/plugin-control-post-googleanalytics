<?php
require_once 'google-api-php-client/src/Google/autoload.php';

define ( "PROFILE_ID", 126476310 );
define ( "MAX_POSTS", 4 );
class PopularPosts {
	/*
	 * @mehtod: Create a service to connect with google analytics @returns: a google analytics object to use to get information
	 */
	public function getService() {
		// Creates and returns the Analytics service object.

		// Load the Google API PHP Client Library.

		// Use the developers console and replace the values with your
		// service account email, and relative location of your key file.
		//$service_account_email = 'nimble-valve-104722@appspot.gserviceaccount.com';
		$service_account_email = 'maxim-3b1a5@appspot.gserviceaccount.com';

		//$key_file_location = dirname ( __FILE__ ) . '/client_secrets.p12';
		$key_file_location = dirname ( __FILE__ ) . '/maxim-ab691197c81b.p12';


		// Create and configure a new client object.
		$client = new Google_Client ();
		$client->setApplicationName ( "Popular Posts" );
		$analytics = new Google_Service_Analytics ( $client );

		// Read the generated client_secrets.p12 key.
		$key = file_get_contents ( $key_file_location );
		$cred = new Google_Auth_AssertionCredentials ( $service_account_email, array (
				Google_Service_Analytics::ANALYTICS_READONLY
		), $key );
		$client->setAssertionCredentials ( $cred );
		if ($client->getAuth ()->isAccessTokenExpired ()) {
			$client->getAuth ()->refreshTokenWithAssertion ( $cred );
		}

		return $analytics;
	}
	/*
	 * @method: Get the first view of a user @parameters: $analytics = the analytics object to get the view
	 */
	public function getFirstprofileId(&$analytics) {
		// Get the user's first view (profile) ID.

		// Get the list of accounts for the authorized user.
		$accounts = $analytics->management_accounts->listManagementAccounts ();

		if (count ( $accounts->getItems () ) > 0) {
			$items = $accounts->getItems ();
			$firstAccountId = $items [0]->getId ();

			// Get the list of properties for the authorized user.
			$properties = $analytics->management_webproperties->listManagementWebproperties ( $firstAccountId );

			if (count ( $properties->getItems () ) > 0) {
				$items = $properties->getItems ();
				$firstPropertyId = $items [0]->getId ();

				// Get the list of views (profiles) for the authorized user.
				$profiles = $analytics->management_profiles->listManagementProfiles ( $firstAccountId, $firstPropertyId );

				if (count ( $profiles->getItems () ) > 0) {
					$items = $profiles->getItems ();

					// Return the first view (profile) ID.
					return $items [0]->getId ();
				} else {
					throw new Exception ( 'No views (profiles) found for this user.' );
				}
			} else {
				throw new Exception ( 'No properties found for this user.' );
			}
		} else {
			throw new Exception ( 'No accounts found for this user.' );
		}
	}

	/*
	 * @method: Get the five more recent posts
	 */
	public function getPopularPosts() {
		$analytics = self::getService ();
		$profile = self::getFirstProfileId ( $analytics );

		$date = new DateTime ();
		$today = $date->format ( 'Y-m-d' );
		$date->add ( DateInterval::createFromDateString ( 'yesterday' ) );
		$week = date ( 'Y-m-d', strtotime ( '-1 week' ) );

		$optParams = array (
				'max-results' => MAX_POSTS*2, //porque algunos post no funcionan bien
				'dimensions' => 'ga:pageTitle,ga:pagePath',
				'sort' => '-ga:pageviews',
				'filters' => 'ga:pagePath!~^/category/*/'
		);

		$resultados = null;
		try {
			/* Se realiza la consulta con las dimensiones/metricas/filtrso requeridos */
			$resultados = $analytics->data_ga->get ( 'ga:' . PROFILE_ID, $week, $today, 'ga:pageviews', $optParams );
		} catch ( Exception $e ) {
			error_log ( "Error al consultar google analytics" . $e->getMessage () );
		}
		
		return $resultados;
		
	}

    /*
    * @method: validate if and url is show or not in the trending now list
    * @params: $url: the url to validate
    * @retunrs: (Boolean) value for the url
    */
    public static function excludeAdminUrls($url){

		if($url == '/' || strpos($url, '/maxim/web/') !== false)
		{
            return (bool) true;
        }
        return (bool) false;
    }


	/*
	 * @method: get data and load posts by id @returns: the array with the posts ids
	 * @retunrs: the ids of posts, now is bad the ids is bad
	 */
	public function codeShortPopularPosts() {

        $array = array();

        $cached = (bool) false;

        $cached_posts = get_transient('ids_popular_posts_array');

        // if($cached_posts != false){
        //     $array = $cached_posts;
		// 		$cached = (bool) true;
        // }else{

			$data = self::getPopularPosts();
				
			
		

            foreach((array) $data->rows as $key => $value){
			
				if(self::excludeAdminUrls($value[1])){
					continue;
				}
			
				$post_id = url_to_postid($value[1]);	
				if (!$post_id){
					continue;
				}

				array_push($array,$post_id);

			}
		

		
			if (count($array) < MAX_POSTS)
			{
				$array = self::rellenar_con_post_nuevos($array);
			}
            set_transient('ids_popular_posts_array', $array, DAY_IN_SECONDS);

		// }
		 $array= array_slice($array, 1, MAX_POSTS); 
		 //var_dump("davidfelipe--->");var_dump($array);die;

		return $array;
		
	}

	private static function rellenar_con_post_nuevos($post_populares){
		//post nuevos
		$latest_cpt =  (Array) get_posts("post_type=post&numberposts=10");
		//completamos con post nuevos por si los post
		//validos devueltos por Google analitycs no alcanzar a ser  MAX_POSTS
		for ($i=count($post_populares); $i <= MAX_POSTS; $i++){

			    $rand_key  = array_rand($latest_cpt);
				$rand_post = $latest_cpt[$rand_key];
				
				array_push($post_populares,(Integer) $rand_post->ID);
				//quitamos el que ya se uso para rellenar
				unset($latest_cpt[$rand_key]);
		}
		
		return $post_populares;
	}
}
