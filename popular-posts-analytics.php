<?php
include 'PopularPosts.php';

function popular_posts($settings) {

	$pop = new PopularPosts ();
	

	$posts = $pop->codeShortPopularPosts ();
	//var_dump($posts);die;
	// Load post in custom display
	// analytics-post.php
	ob_start ();
	$Metodo = get_option( 'options_origen_noticia' );
	
	$args = array (
			'post_type' => array (
					'post'
			),
			'orderby' => 'post__in',
			'post__in' => $posts
	);

	if($Metodo == "manual" ){		
		$args["post__in"] = get_option( 'options_seleccionar_post' );
	}

	$contador = 0;
	ob_start ();
	print "<div class='pupilar-title'>TRENDING NOW</div>";
	$query = new WP_Query ( $args );
	while ( $query->have_posts () ) :
		$contador ++;
		$query->the_post ();
		print "<div class='counter'>"."<span>".$contador."</span>"."</div>";
		get_template_part ( 'templates/post/analytics-post' );
	endwhile
	;
	$data = ob_get_contents ();
	ob_end_clean ();
	
	print $data;
}
add_shortcode ( 'popular_posts', 'popular_posts' );

