

<?php
/*
	Plugin Name: Control Post Google Analytics
	Description: Muestra los post mas populares segun google analytics
	Author: Mocionsoft Juan Lopez / David Martinez
	Version: 1.0.0
*/
class ControPost_GoogleAnalytics {

    public function __construct() {
        // Hook into the admin menu
        add_action( 'admin_menu', array( $this, 'create_plugin_settings_page' ) );
        add_action( 'admin_init', array( $this, 'add_acf_variables' ) );

        add_filter( 'acf/settings/path', array( $this, 'update_acf_settings_path' ) );
        add_filter( 'acf/settings/dir', array( $this, 'update_acf_settings_dir' ) );

        include_once( plugin_dir_path( __FILE__ ) . 'vendor/advanced-custom-fields/acf.php' );

        $this->setup_options();
    }

    public function update_acf_settings_path( $path ) {
        $path = plugin_dir_path( __FILE__ ) . 'vendor/advanced-custom-fields/';
        return $path;
    }

    public function update_acf_settings_dir( $dir ) {
        $dir = plugin_dir_url( __FILE__ ) . 'vendor/advanced-custom-fields/';
        return $dir;
    }

    public function create_plugin_settings_page() {
    	// Add the menu item and page
    	$page_title = 'Control Post Google Analytics';
    	$menu_title = 'Control Post Google Analytics';
    	$capability = 'manage_options';
    	$slug = 'Control_posts';
    	$callback = array( $this, 'plugin_settings_page_content' );
    	$icon = 'dashicons-admin-plugins';
    	$position = 100;

    	add_menu_page( $page_title, $menu_title, $capability, $slug, $callback, $icon, $position );
    }

    public function plugin_settings_page_content() {
        do_action('acf/input/admin_head');
        do_action('acf/input/admin_enqueue_scripts');

        $options = array(
        	'id' => 'acf-form',
        	'post_id' => 'options',
        	'new_post' => false,
        	'field_groups' => array( 'acf_awesome-options' ),
        	'return' => admin_url('admin.php?page=Control_posts'),
        	'submit_value' => 'Update',
        );
        acf_form( $options );
    }

    public function add_acf_variables() {
        acf_form_head();
    }

    public function setup_options() {

    	if(function_exists("register_field_group"))
		{
			register_field_group(array (
	 		'id' => 'acf_awesome-options',
	 		'title' => 'Awesome Options',
				'fields' => array (
					array (
						'key' => 'field_5b1077bb4498b',
						'label' => 'Noticia automatica s a partir de google analytics o seleccionada manualmente',
						'name' => 'origen_noticia',
						'type' => 'radio',
						'instructions' => 'Noticia automatica s a partir de google analytics o seleccionada manualmente',
						'choices' => array (
							'google_analytics' => 'google_analytics',
							'manual' => 'manual',
						),
						'other_choice' => 0,
						'save_other_choice' => 0,
						'default_value' => 'google_analytics',
						'layout' => 'vertical',
					),
					array (
						'key' => 'field_5b10776a4498a',
						'label' => 'Seleccionar post',
						'name' => 'seleccionar_post',
						'type' => 'relationship',
						'conditional_logic' => array (
							'status' => 1,
							'rules' => array (
								array (
									'field' => 'field_5b1077bb4498b',
									'operator' => '==',
									'value' => 'manual',
								),
							),
							'allorany' => 'all',
						),
						'return_format' => 'object',
						'post_type' => array (
							0 => 'post',
						),
						'taxonomy' => array (
							0 => 'all',
						),
						'filters' => array (
							0 => 'search',
						),
						'result_elements' => array (
							0 => 'post_type',
							1 => 'post_title',
						),
						'max' => 4,
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'post',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'no_box',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}

    }

}
new ControPost_GoogleAnalytics();

include 'popular-posts-analytics.php';
